# LIS4381 Mobile Wet Application Development

## Avery Withers

### Assignment 2 Requirements:

*Three Parts:*

1. Create a mobile recipe app using Android Studio.
2. Provide README.md file with the following requirements below
3. Chapter Questions (Chs 3, 4)

#### README.md file should include the following items:

* Coure title, your name, assingment requirements, as perA2;
* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface;
* Screenshot of skillsets 1, 2, and 3;

#### Assingment 2 README.md link
* https://bitbucket.org/arw18bj/lis4381/src/e38e630a8a3feffe0556837ba450e150c4ab9daf/a2/a2_README.md

#### Assignment 2 Screenshots:

*Screenshot of application's first user interface*:

![First User Interface](img/first_user_interface.png)

*Screenshot of application's second user interface*:

![Second User Interface](img/second_user_interface.png)

#### Skillset Screenshots:

ss1 - Even Or Odd        |ss2 - Largest Integer    |ss3 - Arrays and Loops
:-----------------------:|:-----------------------:|:-----------------------:
![](img/even_or_odd.png)|![](img/largest_of_two_integers.png)|![](img/arrays_and_loops.png)