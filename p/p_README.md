# LIS4381 Mobile Wet Application Development

## Avery Withers

### Project 1 Requirements:

*Three Parts:*

1. Create a business card application
2. Provide README.md file with the following requirements below
3. Chapter Questions (Chs 7, 8)

#### README.md file should include the following items:

* Coure title, your name, assingment requirements, as per A1;
* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface;
* Screenshot of skillsets 7, 8, and 9;

#### Project 1 Screenshots:

*Screenshot of application's first user interface*:

![First User Interface](img/card.png)

*Screenshot of application's second user interface*:

![Second User Interface](img/card_details.png)

#### Skillset Screenshots:

*SS7*:

![SS7](img/7_random_array_data_validation.png)

*SS8*:

![SS8](img/8_largest_three_numbers.png)

*SS9*:

![SS9](img/9_array_runtime_data_validation.png)