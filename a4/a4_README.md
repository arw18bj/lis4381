# LIS4381 Mobile Wet Application Development

## Avery Withers

### Assignment 4 Requirements:

*Three Parts:*

1. Create an online portfolio
2. Provide README.md file with the following requirements below
3. Chapter Questions (Chs 9, 10, 15)

#### README.md file should include the following items:

* Coure title, your name, assingment requirements, as per A1;
* Screenshots as per below examples;
* Link to local lis4381 web app:
    * [A4 Online Portfolio](http://localhost/repos/lis4381 "A4 Online Portfolio")

#### Skillset Screenshots:

*SS10*:

![SS10](img/10.png)

*SS11*:

![SS11](img/11.png)

*SS12*:

![SS12](img/12.png)