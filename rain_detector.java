import java.util.Scanner;

public class Methods
{
  public static void getRequirements()
  {
    System.out.println("Program Requirements:\n" +
                    "1. Capture user=entered input\n" +
                    "2. Based upon day of week, determine if rain will occur. ;)\n" +
                    "3. Notes: Program does case-insensitive comparison.");
  }
  public static void getUserInput()
  {
    String dayOfWeek="";
    String myStr="";
    Scanner sc = new Scanner(System.in);

    System.out.print("Enter day of the week: ");
    dayOfWeek=sc.next();

    System.out.println();

    if(dayOfWeek == "monday")
    {
        System.out.println("Better get an umbrella!");
    }
    else if(dayOfWeek == "tuesday")
    {
        System.out.println("Better get an umbrella!");
    }
    else if(dayOfWeek == "wednesday")
    {
        System.out.println("Better get an umbrella!");
    }
    else if(dayOfWeek == "thursday")
    {
        System.out.println("Better get an umbrella!");
    }
    else if(dayOfWeek == "friday")
    {
        System.out.println("Better get an umbrella!");
    }
    else if(dayOfWeek == "saturday")
    {
        System.out.println("Whoopee! It's the weekend!!!");
    }
    else if(dayOfWeek == "sunday")
    {
        System.out.println("Whoopee! It's the weekend!!!");
    }
    else
    {
        System.out.println("Invalid input, don't be an asshole!")
    }
  }
}