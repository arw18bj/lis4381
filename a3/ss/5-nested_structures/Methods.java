import java.util.Scanner;

public class Methods
{
    //nonvalue-returning method (static requires no object)
    public static void getRequirements()
    {
    //display operational messages
    System.out.println("Developer: Avery Withers");
    System.out.println("Program searches user-entered integer w/in array of integers.");
    System.out.println("Create an array with the following values: 3, 2, 4, 99, -1, -5, 3, 7");
    System.out.println(); //print blank line
    }

    //nonvalue-returning (void) method (static requires no object)
    public static void searchArray()
    {
    int nums[] = {3, 2, 4, 99, -1, -5, 3, 7};
    Scanner sc = new Scanner(System.in);
    int search;

    System.out.print("Array length: " + nums.length);

    System.out.println("\nEnter search value: ");
    search = sc.nextInt();

    System.out.println(); //print blank line
    for (int i = 0; i < nums.length; i++)
        {
            if (nums[i] == search)
            {
                System.out.println(search + " found at index " + i);
            }
            else
            {
                System.out.println(search + " *not* found at index " + i);
            }
        }
    }
}