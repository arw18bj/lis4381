# LIS4381 Mobile Wet Application Development

## Avery Withers

### Assignment 3 Requirements:

*Three Parts:*

1. Create a database for petstores and event ticket calculator mobile app
2. Provide README.md file with the following requirements below
3. Chapter Questions (Chs 5, 6)

#### README.md file should include the following items:

* Coure title, your name, assingment requirements, as per A1;
* Screenshot of ERD;
* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface;
* Screenshots of 10 records for each table
* Links to a3.mwb and a3.sql files
* Screenshot of skillsets 4, 5, and 6;

#### Assingment 3 .mwb and sql links
* mwb file [A3 mwb file](database/a3.mwb "A3 mwb file")

* sql file [A3 sql file](database/a3.sql "A3 sql file")

#### Assignment 3 Screenshots:

*Screenshot of application's first user interface*:

![First User Interface](img/event.png)

*Screenshot of application's second user interface*:

![Second User Interface](img/event_price.png)

*Screenshot of ERD*:

![ERD](img/erd.png)

#### Table Records Screenshots:

*Customer*:

![Customer](img/customer.png)

*Petstore*:

![Petstore](img/petstore.png)

*Pet*:

![Pet](img/pet.png)

#### Skillset Screenshots:

*SS4*:

![SS4](img/decision_structures.png)

*SS5*:

![SS5](img/nested_structures.png)

*SS6*:

![SS6](img/methods.png)