# LIS4381 Mobile Web Application Development

## Avery Withers

### LIS4381 Requirements:

*Course Work Links:*

1. [A1_README.md](a1/a1_README.md "My a1 README.md file")
    - Install AMMPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstztionlocations and myteamquotes)
    - Provide git commands descriptions
2. [A2_README.md](a2/a2_README.md "My a2 README.md file")
    - Create a mobile recipe app using Android Studio
    - Provide screenshot of both the application's user interfaces
    - Provide screenshots of skillsets 1 - 3
3. [A3_README.md](a3/a3_README.md "My a3 README.md file")
    - Create a mobile ticket price calculator (My Event)
    - Provide screenshot of both the application's user interfaces
    - Provide screenshots of skillsets 4-6
    - Create database for Pet Store
    - Provide .wmb and .sql files of Pet Store database
4. [A4_README.md](a4/a4_README.md "My a4 README.md file")
    - Create code for skillsets 10-12
    - Run code for skillsets 10-12
    - Provide screenshots of skillsets 10-12
5. [A5_README.md](a5/a5_README.md "My a5 README.md file")
    - Do skillset 13
    - Do chapter 11, 12, 19 questions
    - Provide screenshot for skillsets 13
6. [P_README.md](p/p_README.md "My project README.md file")
    - Create a business card application
    - Provide screenshot of both the application's user interfaces
    - Provide screenshots of skillsets 7-9